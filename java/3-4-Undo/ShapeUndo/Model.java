// CS 349 ShapeUndo Demo

import java.awt.geom.*;
import java.awt.*;

import javax.swing.undo.*;

import java.lang.reflect.Field;
import java.util.Observable;
import java.util.Hashtable;

// A rectangleshape model that is undoable
public class Model extends Observable {

	UndoManager undoManager = new UndoManager();
	RectUndoable recUndoable;

    Rectangle2D rect = new Rectangle2D.Double(0, 0, 100, 100);

    // absolute position of the shape
    private int absX = 30;
    private int absY = 30;

    // position before dragged
    private int pX = 30;
    private int pY = 30;

	Model() {
	}

	public void updateViews() {
		setChanged();
		notifyObservers();
	}


	//  methods for direct manipulatio
	// - - - - - - - - - - - - - -

	public void translate(int dx, int dy){
        absX += dx;
        absY += dy;
		updateViews();
	}

	public void endEdit(){
		recUndoable = new RectUndoable(pX, pY, absX, absY);
		undoManager.addEdit(recUndoable);
		pX = absX;
		pY = absY;
	}


	// shape methods
	// - - - - - - - - - - - - - -

    // create transformed shape
	public Shape getShape(){
    	// affinetransformation to translate
        AffineTransform t = new AffineTransform();
        t.translate(absX, absY);
        // create translated shape 
        return t.createTransformedShape(rect);
	}


	// undo and redo methods
	// - - - - - - - - - - - - - -

    public void undo(){
    	if(undoManager.canUndo()){
	        try {
	            undoManager.undo();
	        } catch (CannotRedoException ex) {
	        }	    		
			updateViews();			
    	}
    }

    public void redo(){
    	if(undoManager.canRedo()){
	        try {
		        undoManager.redo();
	        } catch (CannotRedoException ex) {
	        }	    		
			updateViews();			
    	}
    }

	public boolean canUndo() {
		return undoManager.canUndo();
	}

	public boolean canRedo() {
		return undoManager.canRedo();
	}


	public class RectUndoable extends AbstractUndoableEdit{

		// position for undo
		public int p_translateX = 0;
		public int p_translateY = 0;

		// position for redo
		public int n_translateX = 0;
		public int n_translateY = 0;


		public RectUndoable(int px, int py, int x, int y){
			// position for undo
			p_translateX = px;
			p_translateY = py;
			// position for redo
			n_translateX = x;
			n_translateY = y;
		}


		public void undo() throws CannotRedoException {
			super.undo();
			absX = p_translateX;
			absY = p_translateY;
			System.out.println("Model: undo location to " + absX + "," + absY);
			updateViews();
		}

		public void redo() throws CannotRedoException {
			super.redo();
			absX = n_translateX;
			absY = n_translateY;
			System.out.println("Model: redo location to " + absX + "," + absY);
			updateViews();
		}

	}
}
