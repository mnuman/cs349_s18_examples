// CS 349 ShapeUndo Demo

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.util.Observable;
import java.util.Observer;

class View extends JPanel implements Observer {

	int lastX, lastY;


	// the model that this view is showing
	private Model model;
	private boolean needStore = false;

	View(Model model_) {

		// set the model
		model = model_;
		model.addObserver(this);

		// create the view UI
		this.setLayout(new FlowLayout());

		this.addMouseListener(new MouseAdapter(){
	        public void mousePressed(MouseEvent e){
	        	lastX = e.getX();
	        	lastY = e.getY();
			}
	        public void mouseReleased(MouseEvent e){
	        	if(needStore){
		        	model.endEdit();
		        	needStore = false;
	        	}
			}
	    });

	    this.addMouseMotionListener(new MouseAdapter(){
	        public void mouseDragged(MouseEvent e){

	            model.translate(e.getX() - lastX, e.getY() - lastY);
	            lastX = e.getX();
	            lastY = e.getY();
	            needStore = true;
	        }
	    });
	}

    // custom graphics drawing 
    public void paintComponent(Graphics g) {
    	super.paintComponent(g); // JPanel paint
    	Graphics2D g2 = (Graphics2D)g;
    	
     	g2.setStroke(new BasicStroke(3));

    	// draw shape already transformed
     	g2.draw(model.getShape());
    } 	

	// Observer interface
	@Override
	public void update(Observable arg0, Object arg1) {
        repaint();
	}
}
