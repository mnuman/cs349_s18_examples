/*
* CS 349 Java Code Examples
*
* CompositionOrder        Demo of different concatenation orders of matrix transforms.
*                    Click the window to change the order.
*
*/
import javax.swing.JFrame;
import javax.swing.JComponent;
import java.awt.*;
import java.awt.geom.*;
import java.lang.Math.*;
import java.awt.event.*;

// create the window and run the demo
public class TransformHittest {
    
    public static void main(String[] args) {
        // create the window        
        Canvas canvas = new Canvas();
        JFrame f = new JFrame("CompositionOrder"); // jframe is the app window
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(500, 700); // window size
        f.setContentPane(canvas); // add canvas to jframe   
        f.setBackground(Color.WHITE);       
        f.setVisible(true); // show the window
    }
} 

class Canvas extends JComponent {

    Point M = new Point(); // mouse point

    AffineTransform AT1;    // transform matrix for shape1
    AffineTransform AT2;    // transform matrix for shape2

    
    // the house shape (model position is centred at top left corner)
    private Polygon shape = new Polygon(new int[] { -50, 50,  50,   0, -50}, 
                                        new int[] { 75,  75, -25, -75, -25}, 5);

    // a larger font for displaying the concatenation order
    private Font font = new Font("SansSerif", Font.PLAIN, 30);

    // the concatenation order
    private int order = 0;

    Canvas() {
        
        // create transformation matrix for shape1
        AT1 = new AffineTransform();
        AT1.translate(350, 100);
        AT1.rotate(Math.toRadians(30));

        // create another transformation matrix for shape2
        AT2 = new AffineTransform();
        AT2.translate(200, 300);
        AT2.rotate(Math.toRadians(30));
        AT2.scale(2, 2);

        this.addMouseMotionListener(new MouseAdapter(){
            public void mouseMoved(MouseEvent e){
                M.x = e.getX();
                M.y = e.getY();
                repaint();
            }
        }); 
        
        System.out.println("click to change transformation composition order");
    }
    
    // custom graphics drawing 
    public void paintComponent(Graphics g) {
        super.paintComponent(g); // JPanel paint
        Graphics2D g2 = (Graphics2D)g;
        
        // save the current transform matrix 
        AffineTransform ATG = g2.getTransform();
        
        // draw the original shape in "model" coordinates
        g2.setColor(Color.BLACK);
        g2.setStroke(new BasicStroke(3));
        g2.drawPolygon(shape.xpoints, shape.ypoints, shape.npoints);
        

        // Shape1                
        g2.setTransform(AT1);    // Use Transform Matrix AT1 for shape1
        g2.setColor(Color.RED);  
        g2.drawPolygon(shape.xpoints, shape.ypoints, shape.npoints);

        // hit testing
        Point MT = new Point();
        try{
            // create an inverse matrix of AT1
            AffineTransform IAT1 = AT1.createInverse();
            // apply the inverse transformation to the mouse position
            IAT1.transform(M, MT);
            // check if original shape contains transformed mouse position
            if (shape.contains(MT.x, MT.y))
                g2.setColor(Color.RED);
            else
                g2.setColor(Color.WHITE);  
            g2.fillPolygon(shape);
        } catch (NoninvertibleTransformException e){
            // error
        }

        // Shape2
        g2.setTransform(AT2);    // Use Transform Matrix AT2 for shape2
        g2.setColor(Color.BLUE);  
        g2.drawPolygon(shape.xpoints, shape.ypoints, shape.npoints);
        // hit test
        try{
            // create an inverse matrix of AT2
            AffineTransform IAT2 = AT2.createInverse();
            // apply the inverse transformation to the mouse position
            IAT2.transform(M, MT);
            // check if original shape contains transformed mouse position
            if (shape.contains(MT.x, MT.y))
                g2.setColor(Color.BLUE);
            else
                g2.setColor(Color.WHITE);  
            g2.fillPolygon(shape);
        } catch (NoninvertibleTransformException e){
            // error
        }
        
        // reset to transform 
        g2.setTransform(ATG);
        
    }
}
